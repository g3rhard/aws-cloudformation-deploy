from setuptools import setup

setup(
    name='aws-cloudformation-deploy',
    version='0.15.2',
    packages=['pipe', ],
    url='https://bitbucket.org/atlassian/aws-cloudformation-deploy',
    author='Atlassian',
    author_email='bitbucketci-team@atlassian.com',
    description='Pipe aws-cloudformation-deploy',
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    install_requires=[
        'pyyaml==5.4',
        'Cerberus==1.3.4',
        'docker==4.2',
        'boto3==1.20.*',
    ]
)
