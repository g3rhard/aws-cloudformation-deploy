# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.15.2

- patch: Reformat table of variables descriptions.

## 0.15.1

- patch: Internal maintenance: bump Cerberus to fix dependency issues.

## 0.15.0

- minor: Add support for optional STACK_NAME. Default value for STACK_NAME: BITBUCKET_REPO_OWNER-BITBUCKET_REPO_SLUG-BITBUCKET_BUILD_NUMBER.

## 0.14.0

- minor: PIPES-1020: Refactor logic of fetch events. Fetch only fresh events instead of all.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update release process.

## 0.13.0

- minor: Bump boto3 version to 1.20.*.

## 0.12.0

- minor: Internal maintenance: update bitbucket-pipes-toolkit to 3.2.0

## 0.11.2

- patch: Internal maintenance: update awscli to v2.

## 0.11.1

- patch: Internal maintenance: Fix dependency package.

## 0.11.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.10.0

- minor: Support AWS OIDC authentication. Environment variables AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY are not required anymore.

## 0.9.2

- patch: Internal maintenance: bump setup.py

## 0.9.1

- patch: Internal maintenance: get rid of AWS sam access keys.

## 0.9.0

- minor: Add support for EXTRA_PARAMETERS variable. It allows to pass all other supported create/update stack parameters.
- minor: Add support for ROLE_ARN variable.
- patch: Add support for IMPORT_COMPLETE status.

## 0.8.1

- patch: Internal maintenance: bump bitbucket-pipe-release.

## 0.8.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.7.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.7.3

- patch: Internal maintenance: Add gitignore secrets.

## 0.7.2

- patch: Automated the test infrastructure setup

## 0.7.1

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.7.0

- minor: Add default values for AWS variables.

## 0.6.4

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.6.3

- patch: Add a warning message about the new version of the pipe available.

## 0.6.2

- patch: Fix adding tags BITBUCKET_BRANCH.

## 0.6.1

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.6.0

- minor: BITBUCKET_BRANCH, BITBUCKET_COMMIT and BITBUCKET_BUILD_NUMBER tags are now added by default when creating the AWS resources

## 0.5.0

- minor: Add support for tags

## 0.4.5

- patch: Update success message with create or update

## 0.4.4

- patch: Update message add link to the stack

## 0.4.3

- patch: Fixed problems with hitting a recursion limit

## 0.4.2

- patch: Internal maintenance: add auto-update package version

## 0.4.1

- patch: Internal maintenance: update pipes toolkit version

## 0.4.0

- minor: Stack events are now displayed in the output

## 0.3.0

- minor: Add support for array variable for CAPABILITIES

## 0.2.1

- patch: Pipe now uses the slim version of the base python docker image to improve performance

## 0.2.0

- minor: Add support for CAPABILITIES parameter

## 0.1.1

- patch: Maintenance changes

## 0.1.0

- minor: Initial release
